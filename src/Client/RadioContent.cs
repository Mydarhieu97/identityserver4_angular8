﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientConsole
{
    public class RadioContent
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public Guid MediaPlanId { get; set; }
        public string MediaType { get; set; }
        public string MediaTypeId { get; set; }
        public Guid SubMediaTypeId { get; set; }
        public string SupplierProductType { get; set; }
        public Guid SupplierId { get; set; }
        public Guid ChannelId { get; set; }
        public Guid Program { get; set; }
        public int AdDuration { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Weekday { get; set; }
        public string Daypart { get; set; }
        public Guid BuyingModel { get; set; }
        public int Volume { get; set; }
        public string Grp { get; set; }
        public Guid SpotTypeId { get; set; }
        public decimal BaseRate { get; set; }
        public decimal BaseCost { get; set; }
        public string ClientCostDiscount { get; set; }
        public string ClientCostLoading { get; set; }
        public decimal ClientCostNetCost { get; set; }
        public decimal ClientCostOtherCost { get; set; }
        public string ClientCostAgencyFee { get; set; }
        public string ClientCostTax1 { get; set; }
        public string ClientCostTax2 { get; set; }
        public decimal ClientCostTotal { get; set; }
        public string SupplierCostDiscount { get; set; }
        public string SupplierCostLoading { get; set; }
        public decimal SupplierCostNetCost { get; set; }
        public decimal SupplierCostOtherCost { get; set; }
        public string SupplierCostCommission { get; set; }
        public Guid SupplierCostTotal { get; set; }
        public Guid TargetAudiences { get; set; }
        public Guid ClientCode { get; set; }
        public Guid BaseCurrency { get; set; }
        public Guid ForeignCurrency { get; set; }
        public Guid CurrencyExchangeRate { get; set; }
        public int Version { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public bool TrackingChanges { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid LastUpdatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
       
    }
}
