import { UserManagerSettings } from 'oidc-client';


export function configLoginNormal(): UserManagerSettings {
    return {
        authority: 'http://localhost:5000',
        client_id: 'angular_spa',
        redirect_uri: 'http://localhost:4200/auth-callback',
        post_logout_redirect_uri: 'http://localhost:4200/home',
        response_type: 'code',
        scope: 'openid profile api',
        filterProtocolClaims: true,
        loadUserInfo: true,
        automaticSilentRenew: true,
        silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'
    };
}


export function configLoginGoogle(): UserManagerSettings {
    return {
        authority: 'http://localhost:5000',
        client_id: 'angular_spa',
        redirect_uri: 'http://localhost:4200/auth-callback',
        post_logout_redirect_uri: 'http://localhost:4200/home',
        response_type: 'code',
        scope: 'openid profile api',
        filterProtocolClaims: true,
        loadUserInfo: true,
        acr_values: 'idp:Google',
        automaticSilentRenew: true,
        silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'
    };
}

export function configLoginFacebook(): UserManagerSettings {
    return {
        authority: 'http://localhost:5000',
        client_id: 'angular_spa',
        redirect_uri: 'http://localhost:4200/auth-callback',
        post_logout_redirect_uri: 'http://localhost:4200/home',
        response_type: 'code',
        scope: 'openid profile api',
        filterProtocolClaims: true,
        loadUserInfo: true,
        acr_values: 'idp:Facebook',
        automaticSilentRenew: true,
        silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'
    };
}

export function configLoginMicrosoft(): UserManagerSettings {
    return {
        authority: 'http://localhost:5000',
        client_id: 'angular_spa',
        redirect_uri: 'http://localhost:4200/auth-callback',
        post_logout_redirect_uri: 'http://localhost:4200/home',
        response_type: 'code',
        scope: 'openid profile api',
        filterProtocolClaims: true,
        loadUserInfo: true,
        acr_values: 'idp:Microsoft',
        automaticSilentRenew: true,
        silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'
    };
}
