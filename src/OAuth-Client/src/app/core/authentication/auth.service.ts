import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import { BehaviorSubject } from 'rxjs';

import { BaseService } from "../../shared/base.service";
import { ConfigService } from '../../shared/config.service';
import * as Oidc from 'oidc-client';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { configLoginNormal, configLoginFacebook, configLoginGoogle, configLoginMicrosoft } from '../IdentityConfig/loginConfig';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  // Observable navItem source
  // tslint:disable-next-line: variable-name
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this._authNavStatusSource.asObservable();

  // config
  private mgr = new Oidc.UserManager(configLoginNormal());
  private mgrFB = new Oidc.UserManager(configLoginFacebook());
  private mgrGG = new Oidc.UserManager(configLoginGoogle());
  private mgrMS = new Oidc.UserManager(configLoginMicrosoft());
  private user: User | null;

  constructor(private http: HttpClient,
              private configService: ConfigService,
              private alertify: AlertifyService) {
    super();

    this.mgr.getUser().then(user => {
      this.user = user;
      this._authNavStatusSource.next(this.isAuthenticated());
    });
  }

  login() {
    return this.mgr.signinRedirect();
  }

  loginFB() {
    return this.mgrFB.signinRedirect();
  }

  loginGG() {
    return this.mgrGG.signinRedirect();
  }

  loginMS() {
    return this.mgrMS.signinRedirect();
  }

  async completeAuthentication() {
    this.user = await this.mgr.signinRedirectCallback();
    this._authNavStatusSource.next(this.isAuthenticated());
  }

  register(userRegistration: any) {
    return this.http.post(this.configService.authApiURI + '/account', userRegistration);
    // .pipe(catchError(this.handleError));
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  get authorizationHeaderValue(): string {
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  get name(): string {
    return this.user != null ? this.user.profile.name : '';
  }

  async signout() {
    await this.mgr.signoutRedirect();
  }
}
