import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize, catchError } from 'rxjs/operators'
import { AuthService } from '../../core/authentication/auth.service';
import { UserRegistration } from '../../shared/models/user.registration';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  userRegistration: UserRegistration = { name: '', email: '', password: '' };
  submitted = false;

  constructor(private authService: AuthService,
    private spinner: NgxSpinnerService,
    private alertify: AlertifyService,
    private route: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {

    this.spinner.show();

    this.authService.register(this.userRegistration)
      .pipe(finalize(() => {
        this.spinner.hide();
      }))
      .subscribe(
        () => {
          this.alertify.success('Register Successful');
          this.route.navigate(['/login']);
        }, error => {
          if (error.error.Name != null) {
            this.alertify.error(error.error.Name[0]);
          } else if (error.error.Password != null) {
            this.alertify.error(error.error.Password[0]);
          } else if (error.error[0] != null) {
            this.alertify.error(error.error[0].description);
          }
        }
      );
  }
}
