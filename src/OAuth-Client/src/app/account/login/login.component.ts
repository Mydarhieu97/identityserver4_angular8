import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../../core/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'Login';

  constructor(private authService: AuthService, private spinner: NgxSpinnerService) { }
  login() {
    this.spinner.show();
    this.authService.login();
  }

  loginFaceBook() {
    this.spinner.show();
    this.authService.loginFB();
  }

  loginGoogle() {
    this.spinner.show();
    this.authService.loginGG();
  }

  loginMicrosoft() {
    this.spinner.show();
    this.authService.loginMS();
  }

  ngOnInit() {
  }
}


