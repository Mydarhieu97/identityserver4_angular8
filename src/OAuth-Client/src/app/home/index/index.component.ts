import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/authentication/auth.service';
import { User, UserManager } from 'oidc-client';
import { configLoginNormal } from 'src/app/core/IdentityConfig/loginConfig';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  isAuthenticated: boolean;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.authNavStatus$.subscribe(status => this.isAuthenticated = status);
  }

}
