﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace IdentityServer4AspNetIdentity.Models
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "User's Name")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [JsonProperty("password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Preferred technology")]
        public string Technology { get; set; }

        [Display(Name = "Birth date")]
        public DateTime BirthDate { get; set; }
    }
}
