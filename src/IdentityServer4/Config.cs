﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer4AspNetIdentity
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource("api", "Demo API")
            };


        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // client credentials flow client
                new Client
                {
                    ClientId = "client",
                    ClientName = "Client Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedScopes = { "api" }
                },

                // MVC client using hybrid
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",

                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    RedirectUris = { "http://localhost:5002/signin-oidc" },
                    FrontChannelLogoutUri = "http://localhost:5002/signout-oidc",
                    PostLogoutRedirectUris = { "http://localhost:5002/signout-callback-oidc" },

                    AllowOfflineAccess = true,
                    AbsoluteRefreshTokenLifetime = 2592000,
                    AccessTokenLifetime = 3600,
                    AllowedScopes = { "openid", "profile", "api" }
                },

                //SPA client using Code flow + pkce
                new Client
                {
                    ClientId = "angular_spa",
                    ClientName = "SPA Client",

                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,

                    RequireClientSecret = false,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =           { "http://localhost:4200/auth-callback"  },
                    PostLogoutRedirectUris = { "http://localhost:4200/home" },
                    AllowedCorsOrigins =     { "http://localhost:4200" },

                    AllowOfflineAccess = true,
                    AbsoluteRefreshTokenLifetime = 2592000,
                    AccessTokenLifetime = 3600,
                    AllowedScopes = { "openid", "profile", "api" }
                },

                // JS client using code flow + pkce
                new Client
                {
                    ClientId = "js",
                    ClientName = "JS Client",

                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,

                    RequireClientSecret = false,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris =           { "http://localhost:5003/callback.html"  },
                    PostLogoutRedirectUris = { "http://localhost:5003/index.html" },
                    AllowedCorsOrigins =     { "http://localhost:5003" },

                    AllowOfflineAccess = true,

                    AllowedScopes = { "openid", "profile", "api" }
                }
            };
    }
}